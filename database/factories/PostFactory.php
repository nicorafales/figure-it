<?php
declare(strict_types=1);

/* @var Factory $factory */

use App\Post;
use App\PostContent;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'user_id' => \factory(User::class)->create()->id,
        'title' => $faker->sentence,
        'body' => $faker->paragraphs(),
    ];
});
