<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\UserPostRequest;
use App\Http\Requests\UserPutRequest;
use App\Http\Requests\UserRequest;
use App\User;
use Exception;
use Illuminate\Http\Response;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(User::class, 'user');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::orderBy('id', 'desc')->paginate(5);

        return view('users.index')->with('users', $users);
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return Response
     */
    public function show(User $user)
    {
        return redirect('users');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return Response
     */
    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserPutRequest $request
     * @param User $user
     * @return void
     */
    public function update(UserPutRequest $request, User $user)
    {
        $name = $request->get('name');
        $elevated_by = $request->get('elevated_by');
        $user->update(compact('name', 'elevated_by'));

        return redirect()
            ->route('users.index')
            ->with('flash_message', 'User successfully edited.');
    }
}
