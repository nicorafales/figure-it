<?php
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

/**
 * Class PostContent
 * @package App
 *
 * @property-read string $_id
 */
class PostContent extends Moloquent
{
    use SoftDeletes;

    protected $connection = 'mongodb';
    protected $collection = 'posts_collection';

    protected $fillable = ['body', 'post_id'];


    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id', '_id');
    }
}
