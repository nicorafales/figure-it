<?php
declare(strict_types=1);

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/', 'PostController@index')->name('home');

Route::resource('posts', 'PostController');
Route::resource('posts.comments', 'CommentController');
Route::resource('users', 'UserController')->except(['store', 'create', 'destroy'])->middleware('auth');
