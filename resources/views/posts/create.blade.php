@extends('layouts.app')

@section('title', '| Create New Post')

@section('content')
    <div class="container">

        <h1>Create New Post</h1>
        <hr>

        {{ Form::open(['route' => 'posts.store']) }}

        <div class="form-group">
            {{ Form::label('title', 'Title') }}
            {{ Form::text('title', null, ['class' => 'form-control']) }}
            <br>

            {{ Form::label('body', 'Post content') }}
            {{ Form::textarea('body', null, ['class' => 'form-control']) }}
            <br>

            {{ Form::submit('Create Post', ['class' => 'btn btn-success btn-lg btn-block']) }}
        </div>
        {{ Form::close() }}
    </div>

@endsection
