<?php
declare(strict_types=1);

namespace App;

use BeyondCode\Comments\Traits\HasComments;
use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Jenssegers\Mongodb\Helpers\EloquentBuilder;

/**
 * Class Post
 *
 * @package App
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read string $_id
 * @property-read string $slug
 * @property-read int $user_id
 * @property string $title
 * @property PostContent|null $content
 * @property-read User|null $owner
 * @method bool save()
 * @property string $content_id
 * @property-read Collection|Comment[] $comments
 * @method static bool|null forceDelete()
 * @method static EloquentBuilder|Post newModelQuery()
 * @method static EloquentBuilder|Post newQuery()
 * @method static Builder|Post onlyTrashed()
 * @method static EloquentBuilder|Post query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereContentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereUserId($value)
 * @method static Builder|Post withTrashed()
 * @method static Builder|Post withoutTrashed()
 * @mixin Eloquent
 * @property int $id
 * @property string|null $body
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereId($value)
 */
class Post extends Model
{
    use SoftDeletes, HasComments, HybridRelations;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content_id', 'user_id', 'body',
    ];

    /**
     * A post is created by its owner.
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function content()
    {
        return $this->belongsTo(PostContent::class, 'content_id', '_id');
    }

    /**
     * Get the current connection name for the model.
     * @noinspection PhpMissingParentCallCommonInspection
     * @return string|null
     */
    public function getConnectionName()
    {
        return config('database.default');
    }

    public function setBodyAttribute($value)
    {
        if ($this->content()->exists()) {
            $this->content->body = $value;
        } else {
            $this->content()->associate(PostContent::make(['body' => $value]));
        }
    }

    public function getBodyAttribute()
    {
        if ($this->content()->exists() || ($this->content && $this->content->body)) {
            return $this->content->body;
        }
        return null;
    }
}
