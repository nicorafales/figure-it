<?php
declare(strict_types=1);


return [

    /*
     * The comment class that should be used to store and retrieve
     * the comments.
     * Modified to allow hybrid relationships.
     */
    'comment_class' => App\Comment::class,

    /*
     * The user model that should be used when associating comments with
     * commentators. If null, the default user provider from your
     * Laravel authentication configuration will be used.
     */
    'user_model' => null,

];
