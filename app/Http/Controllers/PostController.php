<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Post;
use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Response;

/**
 * Class PostController
 * @package App\Http\Controllers
 */
class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
        $this->authorizeResource(Post::class, 'post');
    }

    /**
     * Display a listing of posts.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        $posts = Post::orderBy('id', 'desc')->paginate(5);
        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new post.
     *
     * @return Renderable
     */
    public function create(): Renderable
    {
        return view('posts.create');
    }

    /**
     * Store a newly created post in storage.
     *
     * @param PostRequest $request
     * @return Response
     */
    public function store(PostRequest $request)
    {
        $post = Post::make($request->only('title', 'body'));
        $post->user_id = $request->user()->id ?? auth()->id();
        $post->save();

        return redirect()
            ->route('posts.index')
            ->with('flash_message', "Post, '{$post->title}' created");
    }

    /**
     * Display the specified post.
     *
     * @param Post $post
     * @return Renderable
     */
    public function show(Post $post): Renderable
    {
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified post.
     *
     * @param Post $post
     * @return Renderable
     */
    public function edit(Post $post): Renderable
    {
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified post in storage.
     *
     * @param PostRequest $request
     * @param Post $post
     * @return void
     */
    public function update(PostRequest $request, Post $post)
    {
        $updated = $post->update($request->only('title', 'content'));
        //todo: check $updated and redirect on error.

        return redirect()
            ->route('posts.show', $post->id)
            ->with('flash_message', 'Post, ' . $post->title . ' updated');
    }

    /**
     * Remove the specified post from storage.
     *
     * @param Post $post
     * @return void
     * @throws Exception
     */
    public function destroy(Post $post)
    {
        $deleted = $post->delete();
        //todo: check $deleted and redirect on error.

        return redirect()
            ->route('posts.index')
            ->with('flash_message', 'Post deleted successfully');
    }
}
