<div class="card-body" style="padding: 0;">
    {!! Form::open(['method' => 'POST', 'route' => ['posts.comments.store', $post->id]]) !!}
    @if(isset($comment))
        {!! Form::hidden('comment_id', $comment->id) !!}
    @endif
    <div class="form-group" style="margin-bottom: 0.5rem;">
        {!! Form::textarea('body', null, ['cols' => 'auto', 'rows' => 1, 'class' => 'form-control']) !!}
    </div>
    {!! Form::submit('Reply', ['class' => 'btn btn-sm btn-primary']) !!}
    {!! Form::close() !!}
</div>
