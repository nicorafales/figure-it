<?php /** @noinspection ALL */

namespace Tests\Unit;

use App\Post;
use App\User;
use BeyondCode\Comments\Comment;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PostTest extends TestCase
{
    use RefreshDatabase;

    public function test_it_has_an_owner()
    {
        $post = factory(Post::class)->make();
        $this->assertInstanceOf(User::class, $post->owner);
    }

    public function test_it_can_save_auth_user()
    {
        $this->signIn();
        $post = Post::make(['title' => 'A Title', 'content' => 'Some random content...']);
        $post->user_id = auth()->id();

        $this->assertInstanceOf(User::class, $post->owner);
    }

    public function test_it_can_store_a_comment_of_post_owner()
    {
        /** @var Post $post */
        $post = factory(Post::class)->create();
        $post->comment('this is a comment from the user');
        $post->save();
        $comment = $post->comments()->first();

        $this->assertInstanceOf(Comment::class, $comment);
        $this->assertTrue($comment->exists());
    }

    public function test_it_can_store_a_comment_from_another_user()
    {
        /** @var Post $post */
        $post = factory(Post::class)->create();
        /** @var User $randomUser */
        $randomUser = factory(User::class)->create();
        $post->commentAsUser($randomUser, 'this is a comment from a different user');
        $comment = $post->comments()->first();

        $this->assertInstanceOf(Comment::class, $comment);
        $this->assertTrue($comment->exists());
    }
}
