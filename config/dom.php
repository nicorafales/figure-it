<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application DOM configs
    |--------------------------------------------------------------------------
    |
    | Custom config for our application.
    | Seeds parameters.
    */

    'seeds' => [
        'admin' => [
            'name' => env('ADMIN_NAME', 'admin'),
            'password' => env('ADMIN_PASSWORD', 777),
            'email' => env('ADMIN_EMAIL', 'admin@test.com'),
            'elevated_at' => now(env('TIMEZONE', 'Pacific/Auckland')),
        ],
    ],
];
