{{-- \resources\views\users\create.blade.php --}}

@extends('layouts.app')

@section('title', '| Add User')

@section('content')
    <div class="container">
        <div class="row">
            <div class="justify-content-center">

                <div class='col-lg-4'>

                    <h1><i class='fa fa-user-plus'></i> Add User</h1>
                    <hr>

                    {{ Form::open(['url' => 'users']) }}

                    <div class="form-group">
                        {{ Form::label('name', 'Name') }}
                        {{ Form::text('name', '', ['class' => 'form-control']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('email', 'Email') }}
                        {{ Form::email('email', '', ['class' => 'form-control']) }}
                    </div>

                    <div class='form-group'>
                        {{ Form::checkbox('elevated_by',  Auth::id()) }}
                        {{ Form::label('Is Admin?') }}<br>
                    </div>

                    <div class="form-group">
                        {{ Form::label('password', 'Password') }}<br>
                        {{ Form::password('password', ['class' => 'form-control']) }}

                    </div>

                    <div class="form-group">
                        {{ Form::label('password', 'Confirm Password') }}<br>
                        {{ Form::password('password_confirmation', ['class' => 'form-control']) }}

                    </div>

                    {{ Form::submit('Add', ['class' => 'btn btn-primary']) }}

                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>

@endsection
