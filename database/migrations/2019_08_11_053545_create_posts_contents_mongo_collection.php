<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Jenssegers\Mongodb\Schema\Blueprint;

class CreatePostsContentsMongoCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::connection('mongodb')->drop(['posts_collection']);

        Schema::connection('mongodb')->create('posts_collection', function (Blueprint $collection) {
            $collection->unsignedBigInteger('post_id');
            $collection->foreign('post_id')->references('id')->on('posts');
            $collection->text('body');
            $collection->timestamps();
            $collection->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::connection('mongodb')->drop(['posts_collection']);
    }
}
