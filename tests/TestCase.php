<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function signIn(User $user = null, $elevated_at = null, $elevated_by = null)
    {
        return $this->actingAs($user ??
            factory(User::class)->create(compact('elevated_at', 'elevated_by')));
    }
}
