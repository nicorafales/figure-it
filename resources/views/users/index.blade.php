{{-- \resources\views\users\index.blade.php --}}
@extends('layouts.app')

@section('title', '| Users')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1><i class="fa fa-users"></i> User Administration</h1>
                <hr>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Date/Time Added</th>
                                <th>Is Admin?</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->created_at->format('F d, Y h:ia') }}</td>
                                    <td><span class="badge badge-info">{{  $user->isAdmin() ? 'yes' : 'no' }}</span></td>
                                    <td>
                                        <div class="form-inline">
                                            @can('update', $user)
                                                <div class="form-group mb-2">
                                                    <i class="fa fa-trash-o"></i><a href="{{ route('users.edit', $user->id) }}"
                                                       class="btn btn-sm btn-primary pull-left" style="margin-right: 3px;">Edit</a>
                                                </div>
                                            @endcan
                                            @can('delete', $user)

                                                <div class="form-group mb-2">
                                                    {!! Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $user->id] ]) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-sm btn-danger']) !!}
                                                    {!! Form::close() !!}
                                                </div>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
