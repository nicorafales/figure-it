{{-- resources\views\errors\list.blade.php --}}
@if (count($errors) > 0)
    <div class="alert alert-danger" style="margin-top: 0.75rem;">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
