<?php
declare(strict_types=1);

namespace App\Observers;

use App\Post;
use DomainException;
use Illuminate\Support\Str;
use Throwable;

/**
 * Class PostObserver
 * @package App\Observers
 */
class PostObserver
{
    /**
     * @param Post $post
     * @return void
     * @throws Throwable
     */
    public function creating(Post $post)
    {
        $post->slug = Str::slug($post->title);

        $contentSaved = $post->content->save();
        throw_unless($contentSaved,
            new DomainException("Content sub-resource could not be persisted properly", 500),
            $post->toArray()
        );
        $post->content_id = $post->content->_id;
    }

    /**
     * Handle the post "created" event.
     *
     * @param Post $post
     * @return void
     */
    public function created(Post $post)
    {
        //
    }

    /**
     * Handle the post "updated" event.
     *
     * @param Post $post
     * @return void
     */
    public function updated(Post $post)
    {
        //
    }

    /**
     * Handle the post "updating" event.
     *
     * @param Post $post
     * @return void
     */
    public function updating(Post $post)
    {
        if ($post->user_id !== Post::find($post->id)->user_id) {
            throw new DomainException("A post cannot be reasigned to a different user");
        }
    }

    /**
     * Handle the post "deleted" event.
     *
     * @param Post $post
     * @return void
     */
    public function deleted(Post $post)
    {
        //
    }

    /**
     * Handle the post "restored" event.
     *
     * @param Post $post
     * @return void
     */
    public function restored(Post $post)
    {
        //
    }

    /**
     * Handle the post "force deleted" event.
     *
     * @param Post $post
     * @return void
     */
    public function forceDeleted(Post $post)
    {
        //
    }
}
