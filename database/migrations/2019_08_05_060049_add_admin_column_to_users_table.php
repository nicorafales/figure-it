<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdminColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->timestamp('elevated_at')->nullable(true);
            $table->unsignedBigInteger('elevated_by')->nullable(true);
            $table->foreign('elevated_by', 'fk_elevated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('fk_elevated_by');
            $table->dropColumn('elevated_at');
            $table->dropColumn('elevated_by');
        });
    }
}
