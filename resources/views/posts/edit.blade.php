@extends('layouts.app')

@section('title', '| Edit Post')

@section('content')
    <div class="container">
        <h1>Edit Post</h1>
        <hr>
        {{ Form::model($post, ['route' => ['posts.update', $post->id], 'method' => 'PUT']) }}
        <div class="form-group">
            {{ Form::label('title', 'Title') }}
            {{ Form::text('title', null, ['class' => 'form-control']) }}<br>

            {{ Form::label('body', 'Post Body') }}
            {{ Form::textarea('body', null, ['class' => 'form-control', 'rows' => 30]) }}<br>

            {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}

            {{ Form::close() }}
        </div>
    </div>

@endsection
