<?php /** @noinspection ALL */

namespace Tests\Feature;

use App\Post;
use Illuminate\Support\Str;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostTest extends TestCase
{
    use RefreshDatabase;

    public function test_guests_can_view_posts()
    {
        $response = $this->get('/');
        $response2 = $this->get('/posts');

        $response->assertOk();
        $response2->assertOk();
    }

    public function test_a_guest_cannot_access_to_post_creation()
    {
        $response = $this->get('/posts/create');

        $this->assertInstanceOf('Illuminate\Auth\AuthenticationException', $response->baseResponse->exception);
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    public function test_a_user_can_create_a_post()
    {
        $this->signIn();
        $response = $this->post('/posts', ['title' => 'A title', 'body' => 'A body']);

        $response->assertRedirect('/posts');
    }

    public function test_a_post_cannot_have_a_title_larger_than_expected()
    {
        $this->signIn();
        $title = Str::random(101);
        $response = $this->post('/posts', ['title' => $title, 'body' => 'A body']);

        $this->assertInstanceOf('Illuminate\Validation\ValidationException', $response->baseResponse->exception);
        $response->assertStatus(302);
        $response->assertRedirect('/');

    }
}
