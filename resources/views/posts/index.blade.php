@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4>Posts</h4>
                Page {{ $posts->currentPage() }} of {{ $posts->lastPage() }}
                @foreach ($posts as $post)
                    <div class="card" style="width: 18rem; margin-top: 2rem;">
                        <div class="card-body">
                            <li style="list-style-type:disc">
                                <h5 class="card-title">{{ $post->title }}</h5>
                                <p class=" card-text teaser">
                                    {{ Str::limit($post->body, 100) }} {{-- Limit teaser to 100 characters --}}
                                </p>
                                <a class="btn btn-outline-info" href="{{ route('posts.show', $post->id ) }}">
                                    Read more
                                </a>
                            </li>
                        </div>
                    </div>
                @endforeach
                <div class="text-center">
                    {!! $posts->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
