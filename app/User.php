<?php
declare(strict_types=1);

namespace App;

use BeyondCode\Comments\Traits\CanComment;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Jenssegers\Mongodb\Eloquent\HybridRelations;

/**
 * App\User
 *
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property Carbon|null $elevated_at
 * @property int|null $elevated_by
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method static bool|null restore()
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereDeletedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|User withoutTrashed()
 * @property-read User|null $elevatedBy
 * @property-read Collection|User[] $madeAdminTo
 * @property-read Collection|Post[] $posts
 * @method static Builder|User whereElevatedAt($value)
 * @method static Builder|User whereElevatedBy($value)
 */
class User extends Authenticatable
{
    use Notifiable, SoftDeletes, HybridRelations, CanComment;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'elevated_by',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'elevated_by', 'elevated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'elevated_at' => 'datetime',
    ];

    /**
     * A user can have many posts.
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function elevatedBy()
    {
        return $this->belongsTo(self::class, 'elevated_by');
    }

    public function madeAdminTo()
    {
        return $this->hasMany(self::class, 'elevated_by');
    }

    /**
     * Get the current connection name for the model.
     * @noinspection PhpMissingParentCallCommonInspection
     * @return string|null
     */
    public function getConnectionName()
    {
        return config('database.default');
    }

    public function isAdmin(): bool
    {
        return !!$this->elevated_at;
    }
}
