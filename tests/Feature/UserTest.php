<?php /** @noinspection ALL */

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_guest_cannot_access_users_resources()
    {
        $response = $this->get('/users');

        $this->assertInstanceOf('Illuminate\Auth\AuthenticationException', $response->baseResponse->exception);
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    public function test_a_user_can_access_users_resources()
    {
        $this->signIn();
        $response = $this->get('/users');

        $response->assertStatus(200);

    }

    public function test_a_non_admin_user_cannot_update_any_other_user()
    {
        $this->signIn();
        $response = $this->put('/users/1', ['name' => 'UpdatedByAccident']);

        $response->assertForbidden();
    }

    public function test_an_admin_can_update_any_user()
    {
        $this->signIn(null, now(), null);
        $anotherUserId = factory(User::class)->create()->id;
        $response = $this->put("/users/$anotherUserId", ['name' => 'Whatever I Like']);

        $response->assertRedirect('/users');
    }
}
