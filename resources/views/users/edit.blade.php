{{-- \resources\views\users\edit.blade.php --}}

@extends('layouts.app')

@section('title', '| Edit User')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class='col-lg-6'>
                <h1><i class='fa fa-user-plus'></i> Edit {{$user->name}}</h1>
                <hr>

                {{ Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'PUT']) }}

                <div class="form-group">
                    {{ Form::label('name', 'Name') }}
                    {{ Form::text('name', null, ['class' => 'form-control']) }}
                </div>

                <div class='form-group'>
                    @can('create', $user)
                        {{ Form::checkbox('elevated_by',  Auth::id()) }}
                    @else
                        {{ Form::checkbox('elevated_by',  Auth::id(), $user->isAdmin(), ['disabled', 'readonly']) }}
                    @endcan
                    {{ Form::label('elevated_by', 'Is Admin?') }}<br>
                </div>

                {{ Form::submit('Add', ['class' => 'btn btn-primary']) }}

                {{ Form::close() }}

            </div>
        </div>
    </div>
@endsection
