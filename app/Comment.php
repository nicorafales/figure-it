<?php
declare(strict_types=1);

namespace App;


use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Jenssegers\Mongodb\Helpers\EloquentBuilder;

/**
 * Class Comment
 *
 * extension from BeyondCodes implementation with adapted
 * functionality to allow working with hybrid
 * relationships with mongoDb models.
 *
 * @package App
 * @property int $id
 * @property string $commentable_type
 * @property string $commentable_id
 * @property string $comment
 * @property bool $is_approved
 * @property int|null $user_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Model|Eloquent $commentable
 * @property-read User|null $commentator
 * @property-read Collection|\BeyondCode\Comments\Comment[] $comments
 * @method static Builder|\BeyondCode\Comments\Comment approved()
 * @method static EloquentBuilder|Comment newModelQuery()
 * @method static EloquentBuilder|Comment newQuery()
 * @method static EloquentBuilder|Comment query()
 * @method static Builder|Comment whereComment($value)
 * @method static Builder|Comment whereCommentableId($value)
 * @method static Builder|Comment whereCommentableType($value)
 * @method static Builder|Comment whereCreatedAt($value)
 * @method static Builder|Comment whereId($value)
 * @method static Builder|Comment whereIsApproved($value)
 * @method static Builder|Comment whereUpdatedAt($value)
 * @method static Builder|Comment whereUserId($value)
 * @mixin Eloquent
 */
class Comment extends \BeyondCode\Comments\Comment
{
    use HybridRelations;

    /**
     * Get the current connection name for the model.
     * @noinspection PhpMissingParentCallCommonInspection
     * @return string|null
     */
    public function getConnectionName()
    {
        return config('database.default');
    }
}
