<ul>
    @foreach ($commentator->comments as $comment)
        <li>
            <div class="card w-50 mt-3">
                <div class="card-body">
                    <div class="card-title"><h5>{{ $comment->commentator->name }} said:</h5></div>
                    <p class="card-text">{{ $comment->comment }}</p>
                    @can('comment', $post)
                        @include('partial.comments.create', ['parent' => $comment])
                    @endcan
                </div>
            </div>
        </li>

        @if($comment->comments->count() > 0)
            @include('partial.comments.index', ['commentator' => $comment])
        @endif
    @endforeach
</ul>
