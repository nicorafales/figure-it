@extends('layouts.app')

@section('title', '| View Post')

@section('content')

    <div class="container">

        <h1>{{ $post->title }}</h1>
        <hr>
        <p class="lead" style="font-family: Georgia, 'Times New Roman';">{{ $post->body }} </p>
        <hr>
        <div class="form-row align-items-center">
            <div class="col-auto">
                {!! Form::open(['method' => 'DELETE', 'route' => ['posts.destroy', $post->id] ]) !!}
                <a href="{{ url()->previous() }}" class="btn btn-outline-info">Back</a>
                @can('update', $post)
                    <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-info" role="button">Edit</a>
                @endcan
                @can('delete', $post)
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                @endcan
                {!! Form::close() !!}
            </div>
        </div>
        @can('comment', $post)
            <br>
            <div class="card-title"><h5>Write a comment...</h5></div>
            @include('partial.comments.create')
        @endcan
        @include('partial.comments.index', ['commentator' => $post])

{{--    crear sub-resource.    --}}

    </div>

@endsection
