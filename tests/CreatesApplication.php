<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Application;
use RuntimeException;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return Application
     */
    public function createApplication()
    {
        /** @var Application $app */
        $app = require __DIR__ . '/../bootstrap/app.php';

        if ($app->configurationIsCached()) {
            throw new RuntimeException("Your configuration is cached. Any variable in phpunit.xml did not take effect.");
        }

        $app->make(Kernel::class)->bootstrap();

        //todo: protect database

        return $app;
    }
}
