<?php

// @formatter:off

/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App {

    use Eloquent;
    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Database\Eloquent\Collection;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Carbon;
    use Jenssegers\Mongodb\Helpers\EloquentBuilder;

    /**
     * Class Comment
     *
     * extension from BeyondCodes implementation with adapted
     * functionality to allow working with hybrid
     * relationships with mongoDb models.
     *
     * @package App
     * @property int $id
     * @property string $commentable_type
     * @property string $commentable_id
     * @property string $comment
     * @property bool $is_approved
     * @property int|null $user_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property-read Model|Eloquent $commentable
     * @property-read User|null $commentator
     * @property-read Collection|\BeyondCode\Comments\Comment[] $comments
     * @method static Builder|\BeyondCode\Comments\Comment approved()
     * @method static EloquentBuilder|Comment newModelQuery()
     * @method static EloquentBuilder|Comment newQuery()
     * @method static EloquentBuilder|Comment query()
     * @method static Builder|Comment whereComment($value)
     * @method static Builder|Comment whereCommentableId($value)
     * @method static Builder|Comment whereCommentableType($value)
     * @method static Builder|Comment whereCreatedAt($value)
     * @method static Builder|Comment whereId($value)
     * @method static Builder|Comment whereIsApproved($value)
     * @method static Builder|Comment whereUpdatedAt($value)
     * @method static Builder|Comment whereUserId($value)
     * @mixin Eloquent
     */
    class Comment extends Eloquent
    {
    }
}

namespace App {

    use Eloquent;
    use Illuminate\Database\Eloquent\Builder;

    /**
     * Class Post
     *
     * @package App
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property Carbon|null $deleted_at
     * @property-read string $_id
     * @property-read string $slug
     * @property-read int $user_id
     * @property string $title
     * @property string $content
     * @property-read User|null $owner
     * @method bool save()
     * @property string $content_id
     * @property-read Collection|Comment[] $comments
     * @method static bool|null forceDelete()
     * @method static EloquentBuilder|Post newModelQuery()
     * @method static EloquentBuilder|Post newQuery()
     * @method static Builder|Post onlyTrashed()
     * @method static EloquentBuilder|Post query()
     * @method static bool|null restore()
     * @method static Builder|Post whereContentId($value)
     * @method static Builder|Post whereCreatedAt($value)
     * @method static Builder|Post whereDeletedAt($value)
     * @method static Builder|Post whereSlug($value)
     * @method static Builder|Post whereTitle($value)
     * @method static Builder|Post whereUpdatedAt($value)
     * @method static Builder|Post whereUserId($value)
     * @method static Builder|Post withTrashed()
     * @method static Builder|Post withoutTrashed()
     * @mixin Eloquent
     */
    class Post extends Eloquent
    {
    }
}

namespace App {

    use Eloquent;

    /**
     * App\User
     *
     * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
     * @method static Builder|User newModelQuery()
     * @method static Builder|User newQuery()
     * @method static Builder|User query()
     * @mixin Eloquent
     * @property int $id
     * @property string $name
     * @property string $email
     * @property Carbon|null $email_verified_at
     * @property string $password
     * @property string|null $remember_token
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property Carbon|null $deleted_at
     * @property Carbon|null $elevated_at
     * @property int|null $elevated_by
     * @method static bool|null forceDelete()
     * @method static Builder|User onlyTrashed()
     * @method static bool|null restore()
     * @method static Builder|User whereCreatedAt($value)
     * @method static Builder|User whereDeletedAt($value)
     * @method static Builder|User whereEmail($value)
     * @method static Builder|User whereEmailVerifiedAt($value)
     * @method static Builder|User whereId($value)
     * @method static Builder|User whereName($value)
     * @method static Builder|User wherePassword($value)
     * @method static Builder|User whereRememberToken($value)
     * @method static Builder|User whereUpdatedAt($value)
     * @method static Builder|User withTrashed()
     * @method static Builder|User withoutTrashed()
     * @property-read User|null $elevatedBy
     * @property-read Collection|User[] $madeAdminTo
     * @property-read Collection|Post[] $posts
     * @method static Builder|User whereElevatedAt($value)
     * @method static Builder|User whereElevatedBy($value)
     */
    class User extends Eloquent
    {
    }
}

