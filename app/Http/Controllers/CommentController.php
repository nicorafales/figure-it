<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Post $post
     * @return Response
     */
    public function index(Post $post)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Post $post
     * @return Response
     */
    public function store(Request $request, Post $post)
    {
        $user = User::find(auth()->id());
        if ($request->get('comment_id')) {
            $comment = Comment::find($request->get('comment_id'));
            $comment->commentAsUser($user, $request->get('body'));
        } else {
            $post->commentAsUser($user, $request->get('body'));
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param Post $post
     * @param Comment $comment
     * @return Response
     */
    public function show(Post $post, Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Post $post
     * @param Comment $comment
     * @return Response
     */
    public function update(Request $request, Post $post, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Post $post
     * @param Comment $comment
     * @return Response
     */
    public function destroy(Post $post, Comment $comment)
    {
        //
    }
}
